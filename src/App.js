import react, { useState } from 'react';
//import './App.css';

function App() {
  const [todos, setTodos] = useState([])
  const [todo, setTodo] = useState("")
  const [todoEditing, setTodoEditing] = useState(null)
  const [editingText, setEditingText] = useState("")

  function handleSubmit(e) {
    e.preventDefault();

    const newTodo = {
      id: new Date().getTime(),
      text: todo,
      completed: false,
    }

    setTodos([...todos].concat(newTodo))
    setTodo("")
  }

  function deleteTodo(id) {
    const updatedTodo = [...todos].filter((todo) => todo.id !== id)

    setTodos(updatedTodo)
  }

  function editTodo(id){
    const updatedTodo = [...todos].map((todo) => {
      if(todo.id === id){
        todo.text = editingText
      }
      return todo
    })

    setTodos(updatedTodo)
    setTodoEditing(null)
    setEditingText("")
  }

  return (
    <div className="App">
      <form onSubmit={handleSubmit}>
        <input type="text" onChange={(e) => setTodo(e.target.value)} value={todo} />
        <button type="submit">Add Todo</button>
      </form>


      {
        todos.map((todo) => <div key={todo.id}>

          {todoEditing === todo.id ? 
          (<input
            type="text"
            onChange={(e) => setEditingText(e.target.value)}
            value={editingText}
          />) 
          : 
          (<div> {todo.text} </div>)}
          

          <button onClick={() => deleteTodo(todo.id)}>Delete</button>
          <button onClick={() => setTodoEditing(todo.id)} >Edit Todo</button>
          <button onClick={() => editTodo(todo.id)} >Update Todo</button>
        </div>)}
    </div>
  );
}

export default App;
